# bucket-ct-streamer

Cloud bucket streamer that leverages Certificate Transparency (CT) broadcast to discover newly created cloud buckets associated with TLS/SSL certificates issued.  